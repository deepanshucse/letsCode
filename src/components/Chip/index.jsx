import React from 'react';
import './styles.css';

const Chip = ({ label,style }) => <p style={style} className='chip'>{label}</p>;

export default Chip;
