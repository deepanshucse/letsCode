import React from "react";
import {
  useColorMode,
} from "@chakra-ui/react";


const OutputDetails = ({ outputDetails }) => {
  const { colorMode, toggleColorMode } = useColorMode();
  let classes = colorMode === 'dark' ? "font-semibold px-2 py-1 rounded-md bg-gray-100 text-black": "font-semibold px-2 py-1 rounded-md bg-gray-100"
  return (
    <div className="metrics-container mt-4 flex flex-col space-y-3">
      <p className="text-sm">
        Status:{" "}
        <span className={classes}>
          {outputDetails?.status?.description}
        </span>
      </p>
      <p className="text-sm">
        Memory:{" "}
        <span className={classes}>
          {outputDetails?.memory}
        </span>
      </p>
      <p className="text-sm">
        Time:{" "}
        <span className={classes}>
          {outputDetails?.time}
        </span>
      </p>
    </div>
  );
};

export default OutputDetails;
