let url = process.env.REACT_APP_BACKEND_URL;
let uploadUrl = process.env.REACT_APP_UPLOAD_URL;
console.log(url);

export default {
    url: url,
    upload: uploadUrl,
}
