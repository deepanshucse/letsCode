import { combineReducers } from 'redux';
import {loginReducer, userReducer} from './Reducers';

export default combineReducers({
    loginStatus: loginReducer,
    userDetails: userReducer
});