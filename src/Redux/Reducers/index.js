import {loginReducer} from './CommonReducer'
import { userReducer } from './userReducer'

export {
    loginReducer,
    userReducer
}